<?php
require "mysql_connect.php";
require "functions.php";

DEFINE('ENTRIES_PER_PAGE', 20);

// if we came here from edit_ip.php then edit corresponding entry
if (isset($_GET[editIpAddress])) {
	$IpAddrNum=mysql_query("SELECT INET_ATON('".$_GET[editIpAddress]."') AS ipaddr_num") or die(mysql_error());
	$IpAddrNum=mysql_fetch_assoc($IpAddrNum);
	$IsDivided=0;
	if ($_GET[editStatus]!=0) {
		$IsDivided=assignSubnet($IpAddrNum["ipaddr_num"], prefixlenToMask($_GET[editPrefixLenght]), $_GET[editRegion_id], $_GET[editStatus]);
	}
        if ($IsDivided > 0) {
                $_GET[editStatus]==0?$EditStatus=1:$EditStatus=$_GET[editStatus]; //subnetted, but forgot to change status
        } else $EditStatus=$_GET[editStatus];
	$_GET[ipAddress]=$_GET[editIpAddress];
        $_GET[region_id]=$_GET[editRegion_id];
	mysql_query("UPDATE ipv4 SET status=".$EditStatus.", description='".$_GET[editDescription]."', user='".$_SERVER[PHP_AUTH_USER]."', date=curdate() 
				WHERE ipaddr=".$IpAddrNum["ipaddr_num"]." AND region_id=".$_GET[editRegion_id]) or die(mysql_error());
	if ($EditStatus==0) {
		aggregateSubnets($IpAddrNum["ipaddr_num"], $_GET[editRegion_id]);
	}
}

// check and insert entry if we came from add_ip.php
if ((isset($_GET[addIpAddress])) && ($_GET[addIpAddress]!=="")) {
	if ((isset($_GET[addPrefixLenght])) && ($_GET[addPrefixLenght]!=="")) {
		if ((isset($_GET[addRegion_id])) && ($_GET[addRegion_id]!=="")) {
			$IpAddrNum=mysql_query("SELECT (INET_ATON('".$_GET[addIpAddress]."') & ".prefixlenToMask($_GET[addPrefixLenght]).") 
							AS ipaddr_num") or die(mysql_error());
			$IpAddrNum=mysql_fetch_assoc($IpAddrNum);
			$MyIpAddr=$IpAddrNum["ipaddr_num"];
			$MyMask=prefixlenToMask($_GET[addPrefixLenght]);
			$MyRegionId=$_GET[addRegion_id];
			$Result = mysql_query("SELECT * FROM ipv4 WHERE ((".$MyIpAddr." = (ipaddr & ".$MyMask.")) OR ((".$MyIpAddr." & mask) = ipaddr))
                                                AND region_id=".$MyRegionId) or die(mysql_error());
			if (mysql_num_rows($Result)==0) {
				$IsDuplicate = mysql_query("SELECT ipaddr FROM ipv4  
								WHERE ((".$MyIpAddr." = (ipaddr & ".$MyMask.")) OR ((".$MyIpAddr." & mask) = ipaddr)) 
								AND region_id <> ".$MyRegionId) or die(mysql_error());
				mysql_query("UPDATE ipv4 SET is_duplicate=1
                                                                WHERE ((".$MyIpAddr." = (ipaddr & ".$MyMask.")) OR ((".$MyIpAddr." & mask) = ipaddr))
                                                                AND region_id <> ".$MyRegionId) or die(mysql_error());
				mysql_num_rows($IsDuplicate)==0?$IsDuplicate=0:$IsDuplicate=1;
				mysql_query("INSERT INTO ipv4 (ipaddr, ipaddr_str, mask, mask_str, region_id, is_duplicate, 
								status, user, date)
								VALUES (".$MyIpAddr.", INET_NTOA(ipaddr), 
									".$MyMask.", INET_NTOA('".$MyMask."'), ".$MyRegionId.", 
									".$IsDuplicate.", 0,
									 '".$_SERVER[PHP_AUTH_USER]."', curdate())") 
									or die(mysql_error());
				aggregateSubnets($MyIpAddr, $MyRegionId);
			} else {
				echo "Error! New entry overlaps with these entries:";
			}
			$_GET[ipAddress]=$_GET[addIpAddress];
			$_GET[region_id]=$_GET[addRegion_id];
			$_GET[prefixLenght]=$_GET[addPrefixLenght];
		}
	}
}

?>


<html>
<head>
        <!-- CSS -->
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />

</head>
<body>
       <!-- <head>
                <title>ipdb Search results:</title>
        </head>
	<h1>ipdb </h1> -->
<div id="main">
        <!-- Header -->
        <div id="header">
                <div id="header-in">
                        <h1>ipdb</h1>
                </div>
        </div>
        <!-- Header end -->

        <!-- Menu -->
        <div id="menu-box" class="cleaning-box">
                <ul id="menu">
                        <li class="first"><a href="index.php" class="active">New Search</a></li>
                        <li><a href="addip.php">Add new IP supernet</a></li>
                        <li><a href="addregion.php">Add new region</a></li>
                </ul>
        </div>
        <!-- Menu end -->

<hr class="noscreen" />

<div id="content">
        <div id="content-box">
        <div id="content-box-in-left">
        <div id="content-box-in-left-in">
<?php
//make query string in accordance with GET parameters
	if ((isset($_GET[ipAddress])) && ($_GET[ipAddress]!=="")) { 
		if ((isset($_GET[prefixLenght])) && ($_GET[prefixLenght]!=="")) {
			$SearchCriteria = "WHERE (ipv4.ipaddr=(INET_ATON('".$_GET[ipAddress]."') & ipv4.mask) 
						OR INET_ATON('".$_GET[ipAddress]."')=(ipv4.ipaddr & ".prefixlenToMask($_GET[prefixLenght])."))";
		} else {
			$SearchCriteria = "WHERE ipv4.ipaddr=(INET_ATON('".$_GET[ipAddress]."') & ipv4.mask)";
		}
	}
	if ((isset($_GET[prefixLenght])) && ($_GET[prefixLenght]!=="")) {
		if ((isset($_GET[compare_operator])) && ($_GET[compare_operator]!=="")) {
			isset($SearchCriteria)?$SearchCriteria.=" AND ":$SearchCriteria="WHERE ";
                	$SearchCriteria .= "ipv4.mask".$_GET[compare_operator].prefixlenToMask($_GET[prefixLenght]);
		}
	}
	if ((isset($_GET[region_id])) && ($_GET[region_id]!=="")) {
		isset($SearchCriteria)?$SearchCriteria.=" AND ":$SearchCriteria="WHERE ";
		$SearchCriteria .= "ipv4.region_id=".$_GET[region_id];	
        }
	if ((isset($_GET[status])) && ($_GET[status]!=="")) {
                isset($SearchCriteria)?$SearchCriteria.=" AND ":$SearchCriteria="WHERE ";
                $SearchCriteria .= "ipv4.status=".$_GET[status];
        }
	if ((isset($_GET[description])) && ($_GET[description]!=="")) {
                isset($SearchCriteria)?$SearchCriteria.=" AND ":$SearchCriteria="WHERE ";
                $SearchCriteria .= "ipv4.description LIKE '%".$_GET[description]."%'";
        }
	if ((isset($_GET[user])) && ($_GET[user]!=="")) {
                isset($SearchCriteria)?$SearchCriteria.=" AND ":$SearchCriteria="WHERE ";
                $SearchCriteria .= "ipv4.user LIKE '%".$_GET[user]."%'";
        }
	if (isset($SearchCriteria)) {
		$SearchCriteria.=" AND region.region_id=ipv4.region_id AND status.status=ipv4.status";
	} else {
		$SearchCriteria="WHERE region.region_id=ipv4.region_id AND status.status=ipv4.status";
	}
	isset($_GET[entrynum])?$EntryNum=$_GET[entrynum]:$EntryNum=0;
//echo "DEBUG: EntryNum=".$EntryNum;
	$QueryString = "SELECT ipv4.ipaddr, ipv4.ipaddr_str, ipv4.mask, ipv4.mask_str, ipv4.region_id, region.region_name, ipv4.is_duplicate, status.status_name, ipv4.description, ipv4.user, ipv4.date FROM ipv4, status, region ".$SearchCriteria;
//echo "DEBUG: $QueryString";	
	$Result = mysql_query($QueryString) or die(mysql_error());
	$TotalNumRows = mysql_num_rows($Result);
if ($TotalNumRows>0) {
	$Result = mysql_query($QueryString." ORDER BY region.region_name, ipv4.mask, ipv4.ipaddr 
						LIMIT ".$EntryNum.", ".ENTRIES_PER_PAGE) or die(mysql_error());
	$NumRows = mysql_num_rows($Result);
?>
	<table border="1">
		<tr>
		<th>IP Address</th>
		<th>Mask</th>
		<th>Region</th>
                <th>Status</th>
                <th>Description</th>
                <th>User</th>
                <th>Date</th>
		</tr>
<?php
	for (;$Row=mysql_fetch_assoc($Result);) {
		$Row["is_duplicate"]?$CellColour="BGCOLOR=Yellow":$CellColour="";
		echo "<tr ".$CellColour.">";
		echo "<td><A HREF=\"edit_ip.php?ipaddr=".$Row["ipaddr"]."&regionid=".$Row["region_id"]."\">".$Row["ipaddr_str"]."</A></td>";
                echo "<td>".$Row["mask_str"]."</td>";
                echo "<td>".$Row["region_name"]."</td>";
                echo "<td>".$Row["status_name"]."</td>";
                echo "<td>".$Row["description"]."</td>";
                echo "<td>".$Row["user"]."</td>";
                echo "<td>".$Row["date"]."</td>";
		echo "</tr>";		
	}	
?>
	</table>

<?php
	$Query="?ipAddress=".$_GET[ipAddress]."&compare_operator=".$_GET[compare_operator]."&prefixLenght=".$_GET[prefixLenght]."&region_id=".$_GET[region_id]."&status=".$_GET[status]."&description=".$_GET[description]."&user=".$_GET[user];
	//PREVIOUS 20 ENTRIES
	if ($EntryNum > 0) {
		$PrevEntryNum = $EntryNum - ENTRIES_PER_PAGE;
		echo "<A HREF=\"".$_SERVER['PHP_SELF'].$Query."&entrynum=".$PrevEntryNum."\"><< Previous ".ENTRIES_PER_PAGE."</A>";
	} else {
		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	}
	//NEXT 20 ENTRIES
	if (($EntryNum + ENTRIES_PER_PAGE) < $TotalNumRows) {
		$NextEntryNum = $EntryNum + ENTRIES_PER_PAGE;
		echo "&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "<A HREF=\"".$_SERVER['PHP_SELF'].$Query."&entrynum=".$NextEntryNum."\">Next ".ENTRIES_PER_PAGE." >></A>";
	}
} else {
	echo "No matches found.";
}
?>
</div>
</div>
</body>
</html>

