<?php
/*
function divideSubnet ($IpAddr, $NewMask, $RegionId)
{
//update db with divided subnets. Return new ip address or 0 if failed.
$Result = mysql_query("SELECT mask, is_duplicate FROM ipv4 WHERE ipaddr = ".$IpAddr." AND region_id=".$RegionId) or die(mysql_error());
$Result = mysql_fetch_assoc($Result);
$Mask = $Result["mask"];
$IsDuplicate = $Result["is_duplicate"];
if ($NewMask <= $Mask) return 0; //check if NewMask is longer than mask of IpAddr
$InvertIndex = maskToPrefixlen($Mask);
while ($Mask < $NewMask) {
	$Mask = inet_ntos($Mask);
	$Mask[$InvertIndex] = 1; 
	$Mask = inet_ston($Mask);
	mysql_query("INSERT INTO ipv4 (ipaddr, ipaddr_str, mask, mask_str, status, region_id, is_duplicate, date) 
				VALUES ((".$IpAddr." | (1 << ".(31-$InvertIndex).")), 
					INET_NTOA(ipaddr), ".$Mask.", INET_NTOA(mask), 
					0, ".$RegionId.", ".$IsDuplicate.", curdate())") or die(mysql_error());
	$InvertIndex += 1; 
	}
mysql_query("UPDATE ipv4 SET mask=".$Mask.", mask_str=INET_NTOA(mask), status=1, user='".$_SERVER[PHP_AUTH_USER]."', date=curdate() 
			WHERE ipaddr=".$IpAddr." AND region_id=".$RegionId) or die(mysql_error());
return $IpAddr;
}
*/

function assignSubnet ($IpAddr, $NewMask, $RegionId, $StatusId)
{
 $Result = mysql_query("SELECT ipaddr, mask, is_duplicate FROM ipv4 WHERE ((".$IpAddr." = (ipaddr & ".$NewMask.")) OR ((".$IpAddr." & mask) = ipaddr)) 
										AND region_id=".$RegionId." AND status=0") or die(mysql_error());
 if (($Result=mysql_fetch_assoc($Result))===false) return -1; //overlapping idle subnet not found
 if ($NewMask < $Result["mask"]) return -2; //NewMask must be longer or equal than mask of IpAddr
 if ($NewMask==$Result["mask"]) {
	mysql_query("UPDATE ipv4 SET status=".$StatusId.", user='".$_SERVER[PHP_AUTH_USER]."', date=curdate() 
				WHERE ipaddr=".$IpAddr." AND region_id=".$RegionId) or die(mysql_error());
	return $Result["ipaddr"];
 }
 mysql_query("UPDATE ipv4 SET mask=((".$Result["mask"]." >> 1) | ".$Result["mask"]."), mask_str=INET_NTOA(mask) 
			WHERE ipaddr=".$Result["ipaddr"]." AND region_id=".$RegionId) or die(mysql_error());
 mysql_query("INSERT INTO ipv4 (mask, mask_str, ipaddr, ipaddr_str, status, region_id, is_duplicate, date) 
			VALUES (((".$Result["mask"]." >> 1) | ".$Result["mask"]."), INET_NTOA(mask), 
				(".$Result["ipaddr"]." | (1 << (32 - BIT_COUNT(mask)))), INET_NTOA(ipaddr), 
				0, ".$RegionId.", ".$Result["is_duplicate"].", curdate())") or die(mysql_error());
 return assignSubnet ($IpAddr, $NewMask, $RegionId, $StatusId);
}

function aggregateSubnets ($IpAddr, $RegionId)
{
//search DB and aggregate if possible. Return aggregated supernet ipaddr or 0 if not aggregated.
//aggregatable only FREE addresses!
$Result = mysql_query("SELECT mask, ((mask << 1) & mask) AS supermask, status, is_duplicate FROM ipv4 
				WHERE ipaddr=".$IpAddr." AND region_id=".$RegionId) or die(mysql_error());
$Result = mysql_fetch_assoc($Result);
if ($Result === false) return -1; //ipaddr not found
if ($Result["status"] == 1) return -2; //ipaddr is not idle. Can not aggregate.
$AggrMask = $Result["supermask"];
$Mask = $Result["mask"];
$IsDuplicate = $Result["is_duplicate"];
$Result = mysql_query("SELECT (ipaddr & ".$AggrMask.") AS aggripaddr, is_duplicate FROM ipv4 
			WHERE ((ipaddr & ".$AggrMask.") = (".$IpAddr." & ".$AggrMask.")) 
				AND mask=".$Mask." 
				AND region_id=".$RegionId." 
				AND status=0 
				AND ipaddr<>".$IpAddr) or die(mysql_error());
$Result = mysql_fetch_assoc($Result);
if ($Result === false) return -3; //nothing to aggregate with
$AggrIpAddr = $Result["aggripaddr"];
if ($Result["is_duplicate"]) $IsDuplicate = 1;
//DELETE both ipaddr and INSERT aggregated ipaddr
mysql_query("DELETE FROM ipv4 WHERE ((ipaddr & ".$AggrMask.") = (".$IpAddr." & ".$AggrMask.")) 
					AND status=0 
					AND mask=".$Mask."  
					AND region_id=".$RegionId) or die(mysql_error());
$Result = mysql_query("INSERT INTO ipv4 (ipaddr, ipaddr_str, mask, mask_str, region_id, is_duplicate, status, user, date) 
					VALUES (".$AggrIpAddr.", INET_NTOA(ipaddr), ".$AggrMask.", INET_NTOA(mask), 
						".$RegionId.", ".$IsDuplicate.", 0, '".$_SERVER[PHP_AUTH_USER]."', curdate())") or die(mysql_error());
return aggregateSubnets($AggrIpAddr, $RegionId);
}

function findIpAddr ($MyIpAddr, $MyMask, $MyRegionId)
{
//check DB for supernet where MyIpAddr is subnet
//or subnets for which MyIpAddr is supernet
//return supernet entry, or 0 if not found
$Result = mysql_query("SELECT * FROM ipv4 WHERE ((".$MyIpAddr." = (ipaddr & ".$MyMask.")) OR ((".$MyIpAddr." & mask) = ipaddr)) 
						AND region_id=".$MyRegionId) or die(mysql_error());
return $Result;
}

function maskToPrefixlen ($MyMask) 
{
//check format of parameter and convert mask to prefix length
//return -1 if parameter is not correct
$MyMask = inet_ntos($MyMask);
$PrefixLen = 0;
while ($MyMask[$PrefixLen] == 1) {
	$PrefixLen = $PrefixLen + 1;
 }
return $PrefixLen; 
}

function prefixlenToMask ($MyPrefixlen)
{
 $BitNum = 0;
 $Mask = "";
 while ($BitNum < 32) {
 	if ($BitNum < $MyPrefixlen) $Mask .= "1";
	else $Mask .= "0";
	$BitNum += 1;
 }
 return inet_ston($Mask);
}

//convert ip address from num to string binary view
function inet_ntos ($MyIpAddr)
{
 settype ($MyIpAddr, double);
 $Result = sprintf("00000000000000000000000000000000%b", $MyIpAddr); 
 return substr($Result, -32);
}

//convert ip address from string binary view to num
function inet_ston ($MyIpAddr)
{
 $MyIpAddr = "00000000000000000000000000000000".$MyIpAddr;
 $MyIpAddr = substr($MyIpAddr, -32);
 $Pow = 0;
 $Result = 0;
 while ($Pow < 32) {
	$Result = $Result + ($MyIpAddr[31 - $Pow] * pow(2, $Pow));
	$Pow += 1;
 }
 return $Result;
}

function delete_entry ($MyIpAddr, $MyRegionId)
{
 $Result = mysql_query("SELECT mask, is_duplicate FROM ipv4 WHERE ipaddr = ".$MyIpAddr." AND region_id = ".$MyRegionId) or die(mysql_error());
 if (($Result = mysql_fetch_assoc($Result)) === false) return -1;
 $MyMask = $Result["mask"];
 $MyIsDuplicate = $Result["is_duplicate"];
 if ($MyIsDuplicate) {
	$NumOfRegions = mysql_query("SELECT region_id FROM ipv4 WHERE ((".$MyIpAddr." = (ipaddr & ".$MyMask.")) OR ((".$MyIpAddr." & mask) = ipaddr))
							AND region_id <> ".$MyRegionId." 
							AND is_duplicate = 1 
							GROUP BY region_id") or die(mysql_error());
	 if (mysql_num_rows($NumOfRegions) > 0) {
		mysql_query("DELETE FROM ipv4 WHERE ipaddr=".$MyIpAddr." AND region_id = ".$MyRegionId) or die(mysql_error());
		check_duplicate();
	}
 } else {
	mysql_query("DELETE FROM ipv4 WHERE ipaddr=".$MyIpAddr." AND region_id = ".$MyRegionId) or die(mysql_error());
 }
 return 1;
}


function check_duplicate ()
{
// check every duplicate ip if it really has duplicate and correct if not!!!
 $CountCorrected = 0;
 $Result = mysql_query("SELECT ipaddr, mask, region_id FROM ipv4 WHERE is_duplicate=1") or die(mysql_error());
 for (;$Row = mysql_fetch_assoc($Result);) {
	$NextResult = mysql_query("SELECT ipaddr FROM ipv4 WHERE 
							((".$Row["ipaddr"]." = (ipaddr & ".$Row["mask"].")) OR ((".$Row["ipaddr"]." & mask) = ipaddr))
							AND is_duplicate=1 
							AND region_id <> ".$Row["region_id"]) or die(mysql_error());
 	if (mysql_num_rows($NextResult)==0) {
		mysql_query ("UPDATE ipv4 SET is_duplicate=0 WHERE ipaddr = ".$Row["ipaddr"]) or die(mysql_error());
		$CountCorrected += 1;
	}
 }
 return $CountCorrected;
}

?>
