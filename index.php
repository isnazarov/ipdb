<?php
require "mysql_connect.php";
require "functions.php";
?>

<html>
<head>
	<!-- CSS -->
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
	<style>
		table {
			width: auto;
			table-layout: auto;
		}
		td {
			text-align: left;
		}
	</style>
</head>
<body>
<div id="main">
	<!-- Header -->
	<div id="header">
		<div id="header-in">
        		<h1>ipdb</h1>
		</div>
	</div>
	<!-- Header end -->

	<!-- Menu -->
	<div id="menu-box" class="cleaning-box">
		<ul id="menu">
			<li class="first"><a href="#" class="active">Search</a></li>
			<li><a href="addip.php">Add new IP supernet</a></li>
			<li><a href="addregion.php">Add new region</a></li>
		</ul>
	</div>
	<!-- Menu end -->
<hr class="noscreen" />

<div id="content">
	<div id="content-box">
	<div id="content-box-in-left">
	<div id="content-box-in-left-in">
        <form ACTION="result.php" METHOD="get" NAME="search">
	<table>
	<tr>
                <th>IP Address: </th><td><input type="text" name="ipAddress" /> </td>
	</tr>
	<tr>
                <th>Prefix Length: </th><td><select name=compare_operator>
			<option value=></option>
                        <option value="=">==</option>
                        <option value=">=">>=</option>
                        <option value="<="><=</option>
                </select>
                <input type="text" name="prefixLenght" size=2 maxlength=2 value=24 /> </td>
	</tr>
	<tr>
		<th>Region: </th><td><select name=region_id>
			<option value=></option>
<?php
			$Result = mysql_query("SELECT region_id, region_name FROM region ORDER BY region_name") or die(mysql_error());
			for (;$Row = mysql_fetch_assoc($Result);) {
				echo "<option value=".$Row["region_id"].">".$Row["region_name"]."</option>";
			}
?>
		</select></td>
	</tr>
	<tr>
		<th>Status: </th><td><select name=status>
			<option value=></option>
<?php
                        $Result = mysql_query("SELECT status, status_name FROM status") or die(mysql_error());
                        for (; $Row = mysql_fetch_assoc($Result);) {
                                echo "<option value=".$Row["status"].">".$Row["status_name"]."</option>";
                        }
?>
		</select></td>
	</tr>
	<tr>
		<th>Description: </th><td><input type="text" name="description" /></td>
	</tr>
	<tr>
		<th>User: </th><td><input type="text" name="user" /></td>
	</tr>
	</table>
		<input type="submit" name="Search" value="Search"/>
        </form>
<!--
<A HREF="addip.php">Add new IP subnet</A>
<br /><br />
<A HREF="addregion.php">Add new region</A>
-->
	</div>
	</div>
	</div>
</div>
<!-- Footer -->
	<div id="footer">
		<div id="footer-in">
			<p class="footer-left">&copy; <a href="#">ipdb</a>, 2011.</p>
			<p class="footer-right"><a href="http://www.mantisatemplates.com/">Free web templates</a> 
			by <a href="http://www.mantisa.cz/">Mantis-a</a></p>
		</div>
	</div>
	<!-- Footer end -->
</div>
</body>
</html>
