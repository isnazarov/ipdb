
SQL DB strucure:

mysql> show tables;
+-----------------+
| Tables_in_ipdb  |
+-----------------+
| ipv4            |
| region          |
| status          |
+-----------------+


mysql> describe ipv4;
+--------------+------------------+------+-----+---------+-------+
| Field        | Type             | Null | Key | Default | Extra |
+--------------+------------------+------+-----+---------+-------+
| ipaddr       | int(10) unsigned | NO   | PRI | 0       |       |
| ipaddr_str   | varchar(15)      | NO   |     | NULL    |       |
| mask         | int(10) unsigned | NO   |     | NULL    |       |
| mask_str     | varchar(15)      | NO   |     | NULL    |       |
| region_id    | int(8) unsigned  | NO   | PRI | 0       |       |
| is_duplicate | tinyint(1)       | NO   |     | NULL    |       |
| status       | int(11)          | NO   |     | NULL    |       |
| description  | varchar(64)      | YES  |     | NULL    |       |
| user         | varchar(32)      | YES  |     | NULL    |       |
| date         | date             | YES  |     | NULL    |       |
+--------------+------------------+------+-----+---------+-------+


mysql> describe region;
+-------------+-----------------+------+-----+---------+----------------+
| Field       | Type            | Null | Key | Default | Extra          |
+-------------+-----------------+------+-----+---------+----------------+
| region_id   | int(8) unsigned | NO   | PRI | NULL    | auto_increment |
| region_name | varchar(64)     | NO   |     | NULL    |                |
+-------------+-----------------+------+-----+---------+----------------+


mysql> describe status;
+-------------+-----------------+------+-----+---------+-------+
| Field       | Type            | Null | Key | Default | Extra |
+-------------+-----------------+------+-----+---------+-------+
| status      | int(2) unsigned | NO   | PRI | NULL    |       |
| status_name | char(32)        | NO   |     | NULL    |       |
+-------------+-----------------+------+-----+---------+-------+