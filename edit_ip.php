<?php
require "mysql_connect.php";
require "functions.php";
?>

<html>
<head>
        <!-- CSS -->
        <link rel="stylesheet" href="css/style.css" type="text/css" media="screen, projection, tv" />
        <style>
                table {
                        width: auto;
                        table-layout: auto;
                }
                td {
                        text-align: left;
                }
        </style>
</head>
<body>
<div id="main">
        <!-- Header -->
        <div id="header">
                <div id="header-in">
                        <h1>ipdb</h1>
                </div>
        </div>
        <!-- Header end -->

        <!-- Menu -->
        <div id="menu-box" class="cleaning-box">
                <ul id="menu">
                        <li class="first"><a href="index.php" class="active">New Search</a></li>
                        <li><a href="addip.php">Add new IP supernet</a></li>
                        <li><a href="addregion.php">Add new region</a></li>
                </ul>
        </div>
        <!-- Menu end -->
<hr class="noscreen" />
<div id="content">
        <div id="content-box">
        <div id="content-box-in-left">
        <div id="content-box-in-left-in">
<?php
        $Result = mysql_query("SELECT INET_NTOA(ipv4.ipaddr) as ipaddr, ipv4.mask, region.region_name, ipv4.status, ipv4.description FROM ipv4, region
                                WHERE ipv4.ipaddr=".$_GET[ipaddr]." AND ipv4.region_id=".$_GET[regionid]."
                                AND region.region_id=ipv4.region_id") or die(mysql_error());
        $Entry=mysql_fetch_assoc($Result);
?>
        <form ACTION="result.php" METHOD="get" NAME="edit_ip">
		<table>
		<tr>
		<th>Prefix: </th>
		<td>
<?php
        if ($Entry["status"]==0) {
                echo "<input type=\"text\" name=\"editIpAddress\" value=\"".$Entry["ipaddr"]."\"/> /
                        <input type=\"text\" name=\"editPrefixLenght\" size=2 maxlength=2 value=\"".maskToPrefixlen($Entry["mask"])."\"/>";
        } else {
                echo "IP address: ".$Entry["ipaddr"]." /".maskToPrefixlen($Entry["mask"]);
                echo "<input type=\"hidden\" name=\"editIpAddress\" value=\"".$Entry["ipaddr"]."\"/>";
        }
?>
		</td>
		</tr>
		<tr>
<!--            <input type="hidden" name="editIpAddress" value="<?php //echo $_GET[ipaddr];?>"/> -->
                <input type="hidden" name="editRegion_id" value="<?php echo $_GET[regionid];?>"/>
                <th>Region: </th><td><?php echo $Entry["region_name"];?></td>
		</tr>
		<tr>
                <th>Status: </th><td><select name=editStatus>
<?php
                $Result = mysql_query("SELECT status, status_name FROM status") or die(mysql_error());
                for (; $Row = mysql_fetch_assoc($Result);) {
                                $Row["status"]==$Entry["status"]?$Selected=" SELECTED":$Selected="";
                                echo "<option value=".$Row["status"].$Selected.">".$Row["status_name"]."</option>";
                }
?>
                </select>
				</td>
		</tr>
		<tr>
                <th>Description: </th><td><input type="text" name="editDescription" value="<?php echo $Entry["description"];?>"/></td>
		</tr>
		</table>	
                <input type="submit" name="Edit" value="Edit"/>
        </form>
<br /><br />
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
<A HREF="delete_ip.php?<?php echo $_SERVER['QUERY_STRING']?>">COMPLETELY Delete this entry from DB (DO NOT MIX UP WITH IDLE Status!!!)</A>

</div>
<!-- Footer -->
        <div id="footer">
                <div id="footer-in">
                        <p class="footer-left">&copy; <a href="index.html">ipdb</a>, 2011.</p>
                        <p class="footer-right"><a href="http://www.mantisatemplates.com/">Free web templates</a>
                        by <a href="http://www.mantisa.cz/">Mantis-a</a></p>
                </div>
        </div>
        <!-- Footer end -->
</div>
</body>
</html>

